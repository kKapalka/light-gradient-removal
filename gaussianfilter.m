function B = gaussianfilter( img,sigma)

kernelSize=round(size(img,1)/2);
nd = ndims(img);
if size(img,3)==3
    B = img;
    if nd == 3
        for i = 1:3
            % filter each channel of the 2D image
            B(:,:,i) = gaussianfilter(img(:,:,i), sigma);
        end
    else
        for i = 1:3
            % filter each channel of the 3D image
            B(:,:,i,:) = gaussianfilter(img(:,:,i,:), sigma);
        end
    end
    return;
    
end
B = img;
% apply a 1D filter in each direction
for i = 1:nd
    % compute spatial reference
    refSize = (kernelSize - 1) / 2;
    s0 = floor(refSize);
    s1 = ceil(refSize);
    lx = -s0:s1;
    
    % compute normalized kernel
    sigma2 = 2 * sigma .^ 2;
    h = exp(-(lx.^2 / sigma2));
    h = h / sum(h);
    
    % reshape
    newDim = [ones(1, i-1) kernelSize ones(1, nd-i)];
    newDim = newDim([2 1 3:nd]);
    
    h = reshape(h, newDim);
    
    % apply filtering along one direction
    B = imfilter(B, h, 'replicate');
end


