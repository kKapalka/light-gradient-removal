% I = imread('Images\1.jpg');
% I = imread('Images\2.png');
% I = imread('Images\3.png');
% I = imread('Images\4.jpg');
 I = imread('Images\5.png');
% I = imread('Images\6.jpg');
% I = imread('Images\8.png');
subplot(2,2,1)
imshow(I);
title('obraz przed przetworzeniem');

subplot(2,2,2)
bg = imopen(I,strel('disk',7));
I2 = I-bg;
imshow(I2)
title('Gradient usuniety - otwarcie morf.');

subplot(2,2,3)
gaus=gaussianfilter(I,5);
I3 = (I-gaus);
imshow(I3)
title('Gradient usuniety - filtr Gaussa');

subplot(2,2,4)
final = (I2+I3);
imshow(final);
title('Gradient usuniety - polaczenie');